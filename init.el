;;; -*- lexical-binding: t -*-
;;; init.el -- Initialization file
;;; Commentary:
;; This file is used to set up the packages sources and then call
;; an org-babel tangle in order to load a literate configuration.

;;; Code:
;;;; Startup monitoring and optimization
;; This snippet is taken from J. Wiegley's init.el
(defconst emacs-start-time (current-time))

(defvar file-name-handler-alist-old file-name-handler-alist)
(setq package-enable-at-startup nil
      file-name-handler-alist nil
      message-log-max 16384
      gc-cons-threshold 402653184
      gc-cons-percentage 0.6
      auto-window-vscroll nil)

(add-hook 'after-init-hook
	  `(lambda ()
	     (setq file-name-handler-alist file-name-handler-alist-old
		   gc-cons-threshold 800000
		   gc-cons-percentage 0.1)
	     (garbage-collect))
	  t)

;;;; Server initialisation
;; This code ensures a server is running when emacs is started
(require 'server)
(unless (server-running-p)
       (server-start))

;;;; Package.el initialisation
(require 'package)

(setq package-archives '(("org" . "http://orgmode.org/elpa/")
                         ("melpa" . "http://melpa.org/packages/")
                         ("melpa-stable" . "http://stable.melpa.org/packages/")
                         ("gnu" . "https://elpa.gnu.org/packages/")))

(setq package-enable-at-startup nil)
(package-initialize)

;;;; Environment and machine-specific files
(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.emacs.d/")))

(eval-and-compile
  (defun emacs-path (path)
    (expand-file-name path user-init-dir))
  (setq emacs-init-d-folder
    (expand-file-name "config.d" user-init-dir))
  (defun emacs-init-path (path)
    (expand-file-name path emacs-init-d-folder))
  )

(setq after-init-machine-file
      (emacs-path "local_after_init.el"))
(setq before-init-machine-file
      (emacs-path "local_before_init.el"))
(setq secrets-file
      (emacs-path ".private.el"))

;;;; Personal information
(setq user-full-name "Gerry Agbobada")

;;;; Before init local file
(when (file-exists-p before-init-machine-file)
  (load before-init-machine-file))
(when (file-exists-p secrets-file)
  (load secrets-file))

;;;; Add lisp/ folder to load-path
(add-to-list 'load-path (emacs-path "lisp"))

;;;; Elisp functions
;;;;; Move lines up or down
(eval-and-compile
  (defun gagbo/move-line-down ()
    "Move a line down one line in the buffer. Does not wrap-around"
    (interactive)
    (let ((col (current-column)))
      (save-excursion
	(forward-line)
	(transpose-lines 1))
      (forward-line)
      (move-to-column col)))

  (defun gagbo/move-line-up ()
    "Move a line up one line in the buffer. Does not wrap-around."
    (interactive)
    (let ((col (current-column)))
      (save-excursion
	(forward-line)
	(transpose-lines -1))
      (forward-line -2)
      (move-to-column col)))
  )

;;;;; Open specific files
;; These functions are to be used in mappings. Interactive functions are satisfying
;; the =commandp= predicate.
;;;;;; Configuration file
(eval-and-compile
  (defun gagbo/open-config-file ()
    "Open the configuration file. Name is hard-coded in configuration."
    (interactive)
    (find-file (emacs-path "init.el"))))

;;;;;; Org inbox/gtd files
(eval-and-compile
  (defun gagbo/open-inbox-file ()
    "Open the org inbox file. Name is hard-coded in configuration."
    (interactive)
    (find-file (concat org-directory "/inbox.org")))

  (defun gagbo/open-gtd-file ()
    "Open the org GTD file. Name is hard-coded in configuration."
    (interactive)
    (find-file (concat org-directory "/gtd.org")))

  (defun gagbo/open-archive-file ()
    "Open the org GTD archive file. Name is hard-coded in configuration."
    (interactive)
    (find-file (concat org-directory "/archive.org")))

  (defun gagbo/open-someday-file ()
    "Open the org GTD someday file. Name is hard-coded in configuration."
    (interactive)
    (find-file (concat org-directory "/un-jour.org"))))

;;;;; Reload init file
;; This function can then be bound to reload the initialisation
;; file from anywhere. Useful to test new settings and to work in a
;; daemonized emacs
(eval-and-compile
  (defun gagbo/reload-init-file ()
    "Reload the initialization file. Useful to try a new configuration without restarting Emacs."
    (interactive)
    (load-file user-init-file)))

;;;;; Eglot helpers
;;;;;; CCLS bonuses
;; This function is pulled from eglot's wiki

(eval-and-compile
  (defun eglot-ccls-inheritance-hierarchy (&optional derived)
    "Show inheritance hierarchy for the thing at point.
 If DERIVED is non-nil (interactively, with prefix argument), show
 the children of class at point."
    (interactive "P")
    (if-let* ((res (jsonrpc-request
		    (eglot--current-server-or-lose)
		    :$ccls/inheritance
		    (append (eglot--TextDocumentPositionParams)
			    `(:derived ,(if derived t :json-false))
			    '(:levels 100) '(:hierarchy t))))
	      (tree (list (cons 0 res))))
	(with-help-window "*ccls inheritance*"
	  (with-current-buffer standard-output
	    (while tree
	      (pcase-let ((`(,depth . ,node) (pop tree)))
		(cl-destructuring-bind (&key uri range) (plist-get node :location)
		  (insert (make-string depth ?\ ) (plist-get node :name) "\n")
		  (make-text-button (+ (point-at-bol 0) depth) (point-at-eol 0)
				    'action (lambda (_arg)
					      (interactive)
					      (find-file (eglot--uri-to-path uri))
					      (goto-char (car (eglot--range-region range)))))
		  (cl-loop for child across (plist-get node :children)
			   do (push (cons (1+ depth) child) tree)))))))
      (eglot--error "Hierarchy unavailable"))))

;;;;;; Prepend the correct company backend
;; Eglot assumes the company backend is capf, so this function
;; advised on =eglot-ensure= will do help make the assumption true.
(eval-and-compile
  (defun gagbo/prepend-company-capf (&rest r)
    "Prepend company-capf in the company backend list."
    (setq company-backends
	  (cons 'company-capf
		(remove 'company-capf company-backends)))
    ))

;;;;; Create new line
(eval-and-compile
  (defun gagbo/create-newline-and-enter-sexp (&rest _ignored)
    "Open a new brace or bracket expression, with relevant newlines and indent."
    (newline)
    (indent-according-to-mode)
    (forward-line -1)
    (indent-according-to-mode)))

;;;;; GNU Global helpers
(eval-and-compile
  (defun gagbo/gtags-root-dir ()
    "Returns GTAGS root directory or nil if does not exist."
    (with-temp-buffer
      (if (zerop (call-process "global" nil t nil "-pr"))
	  (buffer-substring (point-min) (1- (point-max)))
	nil)))

  (defun gagbo/gtags-update ()
    "Make GTAGS incremental update."
    (call-process "global" nil nil nil "-u"))

  (defun gagbo/gtags-update-hook ()
    "Update GTAGS hook."
    (when (gagbo/gtags-root-dir)
      (gagbo/gtags-update)))

  (defun gagbo/gtags-update-single(filename)
    "Update GTAGS database for changes in a single file"
    (interactive)
    (start-process "update-gtags" "update-gtags" "bash" "-c" (concat "cd " (gagbo/gtags-root-dir) " ; gtags --single-update " filename )))

  (defun gagbo/gtags-update-current-file()
    "Update the tags for the current file if a root is found."
    (interactive)
    (defvar filename)
    (setq filename (replace-regexp-in-string (gagbo/gtags-root-dir) "." (buffer-file-name (current-buffer))))
    (gagbo/gtags-update-single filename)
    (message "GTAGS updated for %s" filename)))

;;;;; Frame size follow resolution
;; The choice for the window size is made according to resolution. It should
;; normally aim for 120 char wide windows, with a minimum of 80 char windows
;; in the worst case.

(eval-and-compile
  (defun gagbo/set-frame-size-according-to-resolution ()
    "Set the frame size according to resolution of screen."
    (interactive)
    (if (display-graphic-p)
	(progn
	  (setq initial-frame-alist '(
				      (left . 25)
				      (top . 25)
				      (tool-bar-lines . 0)))
	  (setq default-frame-alist '(
				      (left . 25)
				      (top . 25)
				      (tool-bar-lines . 0)))
	  (if (> (x-display-pixel-width) 1280)
	      (progn
		(add-to-list 'initial-frame-alist (cons 'width 120))
		(add-to-list 'default-frame-alist (cons 'width 120)))
	    (progn
	      (add-to-list 'initial-frame-alist (cons 'width 80))
	      (add-to-list 'default-frame-alist (cons 'width 80)))
	    )
	  (add-to-list 'initial-frame-alist (cons 'height (/ (- (x-display-pixel-height) 300)
							     (frame-char-height))))))))
;;;; Frame position
;; Use the function or the simple if branch
(gagbo/set-frame-size-according-to-resolution)
;; (if (display-graphic-p)
;;     (progn
;;       (set-frame-size (selected-frame) 120 50)
;;       (set-frame-position (selected-frame) 10 10)))

;;;; Customise custom file
;; To make it work the =custom-set-variables= calls should be moved from the
;; init file to the new custom file just after creating the separate custom file.
(setq custom-file (emacs-path "custom.el"))
(when (file-exists-p custom-file) (load custom-file))

;;;; Logging
(defcustom gagbo/init-logging nil
  "Switch controlling init file logging. When non-nil, a lot of use-package section traversals are printed in Messages."
  :type 'boolean)
(defcustom gagbo/init-require-logging nil
  "Switch controlling init file logging. When non-nil, all require are printed in Messages."
  :type 'boolean)
(when gagbo/init-require-logging
  (advice-add 'require :before (lambda (&rest r) (message "Requiring %s" r))))

;;;; Sane defaults
;; Mostly inspired by https://sam217pa.github.io/2016/09/02/how-to-build-your-own-spacemacs/
;; these "sane defaults" are fused
;; with some of the behaviour I liked in my previous configuration

;;;;; Use lexical binding
(setq-default lexical-binding t)

;;;;; Bars
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(when (display-graphic-p) (set-scroll-bar-mode nil))

;;;;; Backup behaviour
;; Delete silently excess backups
(setq delete-old-versions t)
;; Use version control for backup files
(setq version-control t)
;; Make backup files even in version controlled directories
;; I need to be safe from nuking unstaged work
(setq vc-make-backup-files t)
;; Set backup directories
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")))

;; Name translation for backup files
;; We set up a separate folder, and also enable auto-pruning of old save
;; files. This also mimics the tree structure to avoid too long filenames
;; under Mac/Windows (from
;; http://ergoemacs.org/emacs/emacs_set_backup_into_a_directory.html Xah's Blog)
(setq backup-directory-alist '(("" . "~/.local/share/emacs/saves")))

(defun gagbo/backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* (
        (backupRootDir (emacs-path "emacs-backup/"))
	;; remove Windows driver letter in path, for example, “C:”
        (filePath (replace-regexp-in-string "[A-Za-z]:" "" fpath ))
        (backupFilePath (replace-regexp-in-string "//" "/" (concat backupRootDir filePath "~") ))
        )
    (make-directory (file-name-directory backupFilePath) (file-name-directory backupFilePath))
    backupFilePath
  )
)

(setq make-backup-file-name-function 'gagbo/backup-file-name)

;; Trying to disable Auto-save
;; In case it doesn't work, set a folder to declutter working directory
(setq auto-save-default nil)
(setq auto-save-list-file-prefix "~/.emacs.d/auto-save/")
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))

;;;;; No confirmation for symlinks
(setq vc-follow-symlinks t)

;;;;; Inhibit startup screen
(setq inhibit-startup-screen t)

;;;;; Silent bell
(setq ring-bell-function 'ignore)

;;;;; UTF-8 default
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)
(setq coding-system-for-read 'utf-8 )
(setq coding-system-for-write 'utf-8 )

;;;;; Scrolling
;; Default behaviour when point goes off screen is to scroll until
;; point is in the middle of the screen. This is hard to follow
;; for me, so I change the settings so that emacs only scrolls
;; line by line.

;; =scroll-conservatively= is still 0, but the low values of
;; =scroll-(up|down)-aggressively= ensure the smooth scrolling.
(setq-default scroll-margin 3
	      scroll-conservatively 0
	      scroll-up-aggressively 0.01
	      scroll-down-aggressively 0.01)

;;;;; Sentences end with .
(setq sentence-end-double-space nil)

;;;;; Default fill column
(setq default-fill-column 80)

;;;;; Initial startup message
(setq initial-scratch-message "Startup finished !")

;;;;; Initial buffer
(setq remember-notes-initial-major-mode 'org-mode)
(setq remember-data-file "~/org/notes.org")
(setq initial-buffer-choice 'remember-notes)

;;;;; Initial scratch message
;; Remind the user that the mapping to evaluate the command =eval-last-sexpr= is =^J=
;; This initial scratch message won't show with the choice to use the
;; *notes* buffer for the initial buffer.
(setq initial-scratch-message ";; This buffer is for text that is not saved, and \
for Lisp evaluation.\n\
;; To create a file, visit it with \\[find-file] and enter text in its buffer.\n\
;; To evaluate an S-expr in this buffer, use \\[eval-print-last-sexp].\n\n")

;;;;; Resize pixelwise
;; This is needed to work with kwin-tiling for example
;; See faho/kwin-tiling/issues/6
(setq frame-resize-pixelwise t)

;;;;; Frame title
;; See https://www.gnu.org/software/emacs/manual/html_node/elisp/Mode-Line-Data.html#Mode-Line-Data
;; for details about the structure (and the "" in the list)
(setq-default frame-title-format '("" (multiple-frames "%b ")
				   ("" invocation-name "@" system-name " [")
				   (:eval (projectile-project-name)) ("]")))

;;;;; Modeline tweaks
(column-number-mode t)
(line-number-mode t)
(display-time-mode nil)

;;;;; Remove trailing whitespaces on save
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;;;;; Open URLs with xdg-open or open
(cond ((eq system-type 'darwin)
       (setq browse-url-browser-function 'browse-url-default-macosx-browser))
      (t
       (setq browse-url-browser-function 'browse-url-xdg-open)))

;;;;; Save history
(savehist-mode 1)
(setq savehist-file (emacs-path "etc/savehist")
      history-length 150)

;;;;; Recentf // Bookmarks
(setq recentf-save-file (emacs-path "etc/recentf")
      recentf-max-saved-items 50)
(setq bookmark-default-file (emacs-path "etc/bookmarks"))

;;;;; TRAMP
(setq tramp-default-method "ssh")

;;;;; Eldoc
(setq eldoc-idle-delay 0.1)
;; We also activate eldoc for elisp
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)

;;;;; Package-list-packages functions
;; We add functions to filter the list by status (s new), or filter to
;; see only marked packages.
(defun package-menu-find-marks ()
  "Find packages marked for action in *Packages*."
  (interactive)
  (occur "^[A-Z"))

(defun package-menu-filter-by-status (status)
  "Filter the *Packages* buffer by status."
  (interactive
   (list (completing-read
	  "Status : " '("new" "installed" "dependency" "obsolete"))))
  (package-menu-filter (concat "status:" status)))

(define-key package-menu-mode-map "s" #'package-menu-filter-by-status)
(define-key package-menu-mode-map "a" #'package-menu-find-marks)

;;;;; Debugging defaults
;; Use gdb-many-windows by default and show the main routine at start
(setq gdb-many-windows t
      gdb-show-main t)

;;;;; Help buffers in pop-ups/frames
;; I prefer help messages to come in pop-ups like they do when using flycheck-lsp-ui and
;; lsp-mode.
;; This code **tries** to emulate this behaviour. For the time being it only
;; creates a small window in the bottom 25% of the current frame, but
;; hopefully I'll be able to craft a custom function (or take it from
;; flycheck-lsp-ui) to have the nice overlay

(defvar gagbo/help-temp-buffers '("^\\*Flycheck errors\\*$"
				  "^\\*Help\\*$"
				  "^\\*eglot-help*"
				  "^\\*Colors\\*$"))

(mapc
 (lambda (buffer-regex)
   "Add BUFFER-REGEX to the special pop-up alist."
   (add-to-list 'display-buffer-alist
		`(,buffer-regex
		  (display-buffer-in-side-window
		   display-buffer-pop-up-window)
		  (reusable-frames . visible)
		  (window-height . 0.25)
		  (side . bottom)
		  ))
   )
 gagbo/help-temp-buffers)

;;;;; Echo keystokes faster
(setq echo-keystrokes 0.1)

;;;;; Always show matching parens
(show-paren-mode t)

;;;; Initialise Use-package
(setq use-package-always-ensure t)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package)
  (require 'cl))

;;;; Auto-package update
  (use-package auto-package-update
    :ensure t
    :config
    (setq auto-package-update-delete-old-versions t)
    (setq auto-package-update-hide-results t)
    (auto-package-update-maybe))

;;;; Sub files loading
(load (emacs-init-path "evil.el"))
(load (emacs-init-path "general.el"))
(load (emacs-init-path "which-key.el"))

;; Keybindings are set now, so we can global-set-keys to add our own
(global-set-key (kbd "C-S-k") 'gagbo/move-line-up)
(global-set-key (kbd "C-S-j") 'gagbo/move-line-down)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-c C-k") 'compile)

(load (emacs-init-path "minibuffer.el"))
(load (emacs-init-path "org.el"))
(load (emacs-init-path "flymake.el"))
(load (emacs-init-path "completion.el"))
(load (emacs-init-path "version-control.el"))
(load (emacs-init-path "projects.el"))
(load (emacs-init-path "tools.el"))

(load (emacs-init-path "ui.el"))

(load (emacs-init-path "lsp.el"))
(load (emacs-init-path "languages.el"))

;;;; Finalisation
(let ((elapsed (float-time (time-subtract (current-time)
					  emacs-start-time))))
  (message "Loading %s...done (%.3fs)" load-file-name elapsed))

(add-hook 'after-init-hook
	  `(lambda ()
	     (let ((elapsed
		    (float-time
		     (time-subtract (current-time) emacs-start-time))))
	       (message "Loading %s...done (%.3fs) [after-init]"
			,load-file-name elapsed))) t)

(when (file-exists-p after-init-machine-file)
  (load after-init-machine-file))

;;; init.el ends here
