# My GNU Emacs setup

I used to use literate programming for my Emacs setup, but
it turned out tangling was an unnecessary time sink for me.

I restart emacs a lot, and I only used org mode so I could
have pretty headers. Outshine mode allows to get all the good
headers and comments while having only one `init.el` file with
some subfiles for loading thematic packages together in a
controlled order.

`init.el` and `config.d` are the file and directory to check out.
