;;; -*- lexical-binding: t -*-
;; After having a lot of lagging occurring with lsp-mode and pyls (even after the
;; big refactoring) ; and having some maintenance issues with following unstable
;; melpa, I decided to move to eglot.

;;;; Pyls settings
(setq eglot-python-settings
      '(
	:trace (:server "verbose")
	:configurationSources ("flake8")
	:plugins (
		  :jedi_completion (:enabled t)
		  :jedi_definition (
				    :enabled t
				    :follow_imports t
				    :follow_builtin_imports t)
		  :jedi_hover (:enabled t)
		  :jedi_signature_help (:enabled t)
		  :jedi_symbols (
				 :enabled t
				 :all_scopes t)
		  :pylint_lint (:enabled t)
		  :mypy (:enabled t)
		  :mccabe (
			   :enabled t
			   :threshold 15)
		  :preload (
			    :enabled t
			    :modules nil)
		  :pycodestyle (
				:enabled nil
				:exclude nil
				:filename nil
				:select nil
				:ignore nil
				:hangClosing nil
				:maxLineLength nil)
		  :pydocstyle (
			       :enabled nil
			       :convention "pep257"
			       :addIgnore nil
			       :addSelect nil
			       :ignore nil
			       :select nil
			       :match "(?!test_).*\\.py"
			       :matchDir nil)
		  :pyflakes (:enabled t)
		  :rope_completion (:enabled t)
		  :yapf (:enabled t))
	:rope  (
		:extensionModules nil
		:ropeFolder nil)))

;;;; Use-package invocation
;; I advice the function =eglot-ensure= with a small function which ensures that
;; =completion-at-point-function= source is the first backend in company's list,
;; therefore it should always the preferred backend whenever eglot is started
;; using =eglot-ensure=
(use-package eglot
  :commands (eglot eglot-ensure)
  :init
  (advice-add 'eglot-ensure :before #'gagbo/prepend-company-capf)
  (setq eglot-auto-display-help-buffer t)
  :hook
  ((c++-mode . eglot-ensure)
   (c-mode . eglot-ensure)
   (typescript-mode . eglot-ensure)
   (rust-mode . eglot-ensure)
   (python-mode . eglot-ensure)
   (obj-c-mode . eglot-ensure))
  :config
  (when gagbo/init-logging (message "Eglot/:config"))
  (setq eglot-workspace-configuration '((pyls . eglot-python-settings)))
  :general
  (when gagbo/init-logging (message "Eglot/:general"))
  (despot-def eglot-mode-map
    "==" 'eglot-format)
  (tyrant-def eglot-mode-map
    "l" '(:ignore t :which-key "LSP")
    "l=" '(eglot-format :wk "Format")
    "la" '(eglot-code-actions :wk "Code actions")
    "lr" '(eglot-rename :wk "Rename"))
  )

;;; lsp.el ends here.
