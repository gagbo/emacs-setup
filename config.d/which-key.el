;;; -*- lexical-binding: t -*-
(use-package which-key
  :after evil
  :config
  (when gagbo/init-logging (message "WK/:config"))
  (which-key-mode 1)
  (setq which-key-enable-extended-define-key t)
  (setq which-key-allow-evil-operators t)
  (setq which-key-show-operator-state-maps t))

;;; which-key.el ends here.
