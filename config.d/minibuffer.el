;;; -*- lexical-binding: t -*-
;;; Commentary:
;; Packages which interact / enhance the minibuffer
;; Prime candidates are Ivy or Helm, and FZF for example

;;; Code:
;; Also, the theme of the modelines while in a fzf buffer is terrible,
;; I should investigate
;; about this issue later (is it color theme related ?)
(use-package fzf
  :config
  (when gagbo/init-logging (message "FZF/:config"))
  (with-eval-after-load 'evil
    (evil-global-set-key 'normal (kbd "SPC af") 'fzf)
    (evil-global-set-key 'normal (kbd ",p") 'fzf-directory)))

(use-package ivy
  :after (general evil)
  :init
  (when gagbo/init-logging (message "Ivy/:init"))
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  (setq magit-completing-read-function 'ivy-completing-read)
  (setq projectile-completion-system 'ivy)
  (ivy-mode 1)
  :config
  (when gagbo/init-logging (message "Ivy/:config"))
  (evil-define-key '(normal insert) ivy-minibuffer-map
    (kbd "C-j")   'ivy-next-line
    (kbd "M-j")   'ivy-next-history-element
    (kbd "C-M-j") 'ivy-next-line-and-call
    (kbd "C-k")   'ivy-previous-line
    (kbd "M-k")   'ivy-previous-history-element
    (kbd "C-M-k") 'ivy-previous-line-and-call
    (kbd "C-l")   'ivy-alt-done
    (kbd "M-l")   'ivy-yank-word
    (kbd "RET")   'ivy-done
    (kbd "C-M-l") 'ivy-immediate-done)
  :general
  (when gagbo/init-logging (message "Ivy/:general"))
  (tyrant-def
    "ai" '(:ignore t :which-key "Ivy")
    "aiu" 'ivy-resume))

(use-package swiper
  :after (evil general)
  :general
  (when gagbo/init-logging (message "Swiper/:general"))
  (tyrant-def
    "s"   'swiper
    "ais" 'swiper))
(use-package avy
  :after (general evil)
  :general
  (when gagbo/init-logging (message "Avy/:general"))
  (tyrant-def
    "SPC" '(avy-goto-word-or-subword-1  :which-key "go to char")))
(use-package counsel
  :after (general ivy evil)
  :config
  (counsel-mode 1)
  :general
  (when gagbo/init-logging (message "Counsel/:general"))
  (tyrant-def
    "/"     'counsel-rg
    "air"   'counsel-rg
    "bb"    'counsel-ibuffer))

;;; minibuffer.el ends here.
