;;; -*- lexical-binding: t -*-
;;;; Electric pair
(electric-pair-mode t)

;;;; Company
(use-package company
  :hook (after-init . global-company-mode)
  :after evil
  :init
  (setq company-idle-time 0.1
	company-selection-wrap-around t
	company-show-numbers t
	company-require-match 'never
	company-transformers nil)
  :custom
  (company-require-match nil)
  (company-dabbrev-downcase nil)
  (company-minimum-prefix-length 3)
  (company-tooltip-align-annotations t)
  :config
  (setq company-backends (delete 'company-semantic company-backends))
  (add-to-list 'company-backends 'company-files)
  (evil-define-key 'insert company-mode-map (kbd "C-x C-o") #'company-complete)
  )
(defun my-company-abort ()
  "Gracefully abort company completion."
  (interactive)
  (company-abort)
  (when (and (bound-and-true-p evil-mode)
	     (eq evil-state 'insert))
    (evil-force-normal-state)))

(with-eval-after-load 'company
  (define-key company-active-map (kbd "<escape>") #'my-company-abort)
  (define-key company-active-map (kbd "C-n") #'company-select-next)
  (define-key company-active-map (kbd "C-p") #'company-select-previous)
  (define-key company-search-map (kbd "<escape>") #'company-search-abort))

;;;; Smartparens
(use-package smartparens
  :init
  (when gagbo/init-logging (message "SmartParens/:init"))
  (setq sp-show-pair-from-inside t)
  (setq sp-autoescape-string-quote nil)
  :config
  (when gagbo/init-logging (message "SmartParens/:config"))
  (require 'smartparens-config)
  (sp-local-pair 'c-mode "{" nil :post-handlers
		 '((gagbo/create-newline-and-enter-sexp "RET")))
  (sp-local-pair 'java-mode "{" nil :post-handlers
		 '((gagbo/create-newline-and-enter-sexp "RET"))))

;;;; Multiple cursors
;; I need to learn how to properly use the multiple cursors but it is
;; definitely more powerful than visual block mode. The main advantage of
;; multiple cursors is that I can expand a mark to create a selection to
;; start my cursors from (and that is what I need to learn to do in emacs)
;;
;; OPTIONAL: If you prefer to grab symbols rather than words, use
;; `evil-multiedit-match-symbol-and-next` (or prev).
(use-package evil-multiedit
  :after evil
  :bind (
	 :map evil-visual-state-map
	 ;; Highlights all matches of the selection in the buffer.
	 ("R" . 'evil-multiedit-match-all)
	 ;; Match selected region.
	 ("M-d" . 'evil-multiedit-and-next)
	 ;; Same as M-d but in reverse.
	 ("M-D" . 'evil-multiedit-and-prev)
	 ;; Restore the last group of multiedit regions.
	 ("C-M-D" . 'evil-multiedit-restore)

	 :map evil-normal-state-map
	 ;; Match the word under cursor (i.e. make it an edit region).
	 ;; Consecutive presses will
	 ;; incrementally add the next unmatched match.
	 ("M-d" . 'evil-multiedit-match-and-next)
	 ;; Same as M-d but in reverse.
	 ("M-D" . 'evil-multiedit-match-and-prev)

	 :map evil-insert-state-map
	 ;; Insert marker at point
	 ("M-d" . 'evil-multiedit-toggle-marker-here)

	 :map evil-motion-state-map
	 ;; In visual mode, RET will disable all fields outside
	 ;; the selected region
	 ("RET" . 'evil-multiedit-toggle-or-restrict-region)

	 :map evil-multiedit-state-map
	 ;; RET will toggle the region under the cursor
	 ("RET" . 'evil-multiedit-toggle-or-restrict-region)
	 ;; For moving between edit regions
	 ("C-n" . 'evil-multiedit-next)
	 ("C-p" . 'evil-multiedit-prev)

	 :map evil-multiedit-insert-state-map
	 ;; For moving between edit regions
	 ("C-n" . 'evil-multiedit-next)
	 ("C-p" . 'evil-multiedit-prev))
  :config
  ;; Ex command that allows you to invoke evil-multiedit with a regular expression, e.g.
  (evil-ex-define-cmd "ie[dit]" 'evil-multiedit-ex-match))

;;;; Yasnippet
;; Now that Yasnippet does not bundle snippets anymore, we also load the
;; =yasnippet-snippets= package, which appends its bundle to =yas-snippet-dirs=
(use-package yasnippet
  :after evil
  :init
  (when gagbo/init-logging (message "Yas/:init"))
  (setq yas-snippet-dirs (list (concat user-init-dir "snippets")))
  :config
  (when gagbo/init-logging (message "Yas/:config"))
  (yas-global-mode 1)
  (evil-define-key 'insert yas-minor-mode-map (kbd "\t") 'yas-maybe-expand)
  )
(use-package yasnippet-snippets
  :after yasnippet
  :config
  (when gagbo/init-logging (message "Yas-snippet/:config"))
  (yasnippet-snippets-initialize)
  (yas-reload-all))

;;; completion.el ends here.
