;;; -*- lexical-binding: t -*-
(use-package general
  :demand t
  :init
  (when gagbo/init-logging (message "General/:init"))
  (general-override-mode 1)
  (general-auto-unbind-keys)
  :config
  (when gagbo/init-logging (message "General/:config"))
  (general-create-definer tyrant-def
    :states '(normal visual insert motion emacs)
    :keymaps 'override
    :prefix "SPC"
    :non-normal-prefix "C-:")

  (general-create-definer despot-def
    :states '(normal insert)
    :prefix ","
    :non-normal-prefix "C-,")
  (tyrant-def
    ;; simple command
    "'"         '(ansi-term :which-key "terminal")
    "x"         '(execute-extended-command :which-key "M-x")
    "TAB"       '(switch-to-next-buffer :which-key "next buffer")
    "<backtab>" '(switch-to-prev-buffer :which-key "prev buffer")

    ;; Applications
    "a"   '(:ignore t :which-key "Applications")
    "ar"  'ranger
    "ad"  'dired

    ;; Buffers
    "b"  '(:ignore t :which-key "Buffers")
    "bb" 'ibuffer
    "bC" '(clean-buffer-list :which-key "Kill old buffers")
    "bd" '(evil-delete-buffer :wk "Delete buffer")
    "bo" '(switch-to-buffer-other-window :wk "Open buffer in other window")
    "bN" '(evil-buffer-new :which-key "New buffer in current window")
    "bk" '(kill-buffer :which-key "Kill named buffer")

    "c"  '(:ignore t :wk "Configuration")
    "cR" '(gagbo/reload-init-file :wk "Reload")
    "ce" '(gagbo/open-config-file :wk "Edit")

    ;; Error
    "e" '(:ignore t :which-key "Error Nav")
    "en" '(next-error :which-key "Next error")
    "ep" '(previous-error :which-key "Previous error")

    ;; Files
    "f" '(:ignore t :which-key "Files")
    "fc" '(gagbo/open-config-file :wk "Configuration file")
    "ff" 'find-file
    "fd" 'evil-save-and-close
    "fn" '(remember-notes :wk "Notes file")
    "ft" '(gagbo/open-gtd-file :wk "GTD file")

    ;; Help
    "h"   '(:ignore t :which-key "Help")
    "hf"  'list-faces-display
    "hd"  '(:ignore t :which-key "Describe")
    "hdf" 'describe-function
    "hdk" 'describe-key
    "hdv" 'describe-variable
    "hdF" 'describe-face
    "hi"  'info

    ;; Nav
    "n" '(:ignore t :which-key "Code Nav")
    "nt" 'xref-find-definitions
    "nT" 'xref-find-references

    ;; o is org; do not use

    ;; Project
    "p" '(:ignore t :which-key "Project")

    ;; Quit
    "q"  '(:ignore t :which-key "Quit")
    "qN" '(kill-emacs :wk "Kill it with fire")
    "qQ" '(save-buffers-kill-emacs :wk "Kill Emacs")
    "qq" '(save-buffers-kill-terminal :wk "Keep server")

    ;; Theme
    "T" '(:ignore t :which-key "Theming / appearance")
    "Tl" 'load-theme

    ;; Windows
    "w"  '(:ignore t :which-key "Window")
    "wh" 'evil-window-left
    "wj" 'evil-window-down
    "wk" 'evil-window-up
    "wl" 'evil-window-right
    "wd" 'delete-window
    "ws" 'split-window-below
    "wS" 'split-window-below-and-focus
    "wv" 'split-window-right
    "wV" 'split-window-right-and-focus
    "ww" 'other-window
    "wo" 'delete-other-windows
    "w1" 'window-split-single-column
    "w2" 'window-split-double-columns
    "w3" 'window-split-triple-columns
    "w4" 'window-split-grid))

;;; general.el ends here.
