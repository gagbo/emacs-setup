;;; -*- lexical-binding: t -*-
;;; Commentary:
;; After enabling Flymake in all buffers by default (=flymake-mode= is a function as
;; well as a buffer-local variable), I set up flymake-diagnostic-at-point to stop
;; having to use the mouse to know why Flymake is not happy about something.
;;; Code:
(setq-default flymake-mode 1)
(use-package flymake-diagnostic-at-point
  :after flymake
  :hook
  (flymake-mode . flymake-diagnostic-at-point-mode)
  :custom
  (flymake-diagnostic-at-point-diagnostic-function 'flymake-diagnostic-at-point-popup))
(tyrant-def flymake-mode-map
  "e"   '(:ignore t :which-key "Error Nav (Flymake)")
  "ee"  '(flymake-display-err-menu-for-current-line :wk "Explain error")
  "el"  '(flymake-show-diagnostics-buffer :wk "List errors")
  "en"  '(flymake-goto-next-error :which-key "Next error")
  "ep"  '(flymake-goto-prev-error :which-key "Previous error")
  )

;;; flymake.el ends here.
