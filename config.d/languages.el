;;; -*- lexical-binding: t -*-
;;; Commentary:
;; All the language specific packages

;;; Code:
;;;; C/C++
(add-hook 'c-mode-common-hook #'(lambda () (modify-syntax-entry ?_ "w")))
(use-package cc-mode
  :mode (("\\.cc\\'" . c++-mode)
	 ("\\.cpp\\'" . c++-mode)
	 ("\\.cxx\\'" . c++-mode)
	 ("\\.hpp\\'" . c++-mode)
	 ("\\.hxx\\'" . c++-mode)
	 ("\\.c\\'" . c-mode)
	 ("\\.h\\'" . c-mode)
	 ("\\.java\\'" . java-mode)
	 ("\\.awk\\'" . awk-mode))
  :config
  (when gagbo/init-logging (message "CC-mode/:config"))
  (c-add-style "gagbo/c-style" '("gnu" (c-basic-offset . 8)))
  (setq c-basic-offset 4)
  (setq c-default-style '((java-mode . "java")
			  (awk-mode . "awk")
			  (c-mode . "gagbo/c-style")))
  (define-key c-mode-map  [(tab)] 'company-complete)
  (define-key c++-mode-map  [(tab)] 'company-complete)
  )
;;;;; Clang-format
(use-package clang-format
  :after (cc-mode)
  :config
  (when gagbo/init-logging (message "clang-format/:config"))
  (require 'clang-format)
  (define-key c-mode-map (kbd "M-=") 'clang-format-buffer)
  (define-key c++-mode-map (kbd "M-=") 'clang-format-buffer)
  :general
  (when gagbo/init-logging (message "clang-format/:general"))
  (despot-def c++-mode-map
    "=="  'clang-format-buffer))
;;;;; Modern Cpp font lock
(use-package modern-cpp-font-lock)
;;;;; CCLS
(use-package ccls
  :after (cc-mode)
  :config
  (when gagbo/init-logging (message "ccls/:config"))
  (setq ccls-executable "/etc/soft/ccls/Release/ccls")
  (setq ccls-args '("--log-file=/tmp/ccls.log"))
  (setq ccls-sem-highlight-method nil)
  (ccls-use-default-rainbow-sem-highlight)
  (setq ccls-initialization-options '(:completion (:detailedLabel t)
						  :index (:comments 2)
						  :cacheFormat "msgpack"))
  )
;;;; C#
;; We install the omnisharp implementation in emacs to try to edit files
;; more effectively when working in Windows.
(use-package omnisharp
  :mode "\\.cs\\'"
  :hook (csharp-mode . omnisharp-mode)

  :config
  (when gagbo/init-logging (message "Omnisharp/:config"))
  (eval-after-load 'company
    '(add-to-list 'company-backends #'company-omnisharp))
  (setq indent-tabs-mode nil)
  (setq c-syntactic-indentation t)
  (c-set-style "ellemtel")
  (setq c-basic-offset 4)
  (setq truncate-lines t)
  (setq tab-width 4)
  (setq evil-shift-width 4)

  :general
  (when gagbo/init-logging (message "Omnisharp/:general"))
  (despot-def omnisharp-mode-map
    "rr" 'omnisharp-run-code-action-refactoring
    "cc" 'recompile)
  )

;;;; Go
(use-package go-mode
  :mode ("\\.go\\'" . go-mode)
  :general
  (despot-def go-mode-map
    "==" 'gofmt))
(use-package company-go
  :after (company go-mode)
  :config
  (add-to-list 'company-backends 'company-capf))
;;;; Rust
(use-package rust-mode
  :mode ("\\.rs\\'" . rust-mode)
  :init
  (when gagbo/init-logging (message "Rust/:init"))
  (setq rust-format-on-save t))


(use-package racer
  :after rust-mode
  :config
  (when gagbo/init-logging (message "Racer/:config"))
  ;; Racer is disabled for now
  ;; (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode))
(use-package cargo
  :hook (rust-mode . cargo-minor-mode))
;;;; Python
(use-package python
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode))
;; The Pyvenv package provides a nice command to switch virtual env
;; within emacs. It also provides a hook to restart python processes
;; on change, so this is perfect
(use-package pyvenv
  :after python
  :init
  (when gagbo/init-logging (message "Pyvenv/:init"))
  :config
  (when gagbo/init-logging (message "Pyvenv/:config"))
  (add-hook 'pyvenv-post-activate-hooks #'pyvenv-restart-python)
  :general
  (when gagbo/init-logging (message "Pyvenv/:general"))
  (despot-def python-mode-map
    "v"  '(:ignore t :which-key "Virtual Env")
    "vs" 'pyvenv-workon))
;;;;; Test
;; Python-pytest is installed to get integrated tests.
(use-package python-pytest
  :after (python)
  :init
  (setq python-pytest-executable "python3 -m pytest")
  :general
  (despot-def python-mode-map
    "t" '(python-pytest-popup :wk "Pytest")))

;;;;; Blacken
(use-package blacken
  :after (python)
  :general
  (despot-def python-mode-map
    "=b" '(blacken-buffer :wk "Blacken the buffer")))

;;;;; Isortify
(use-package isortify
  :after (python)
  :general
  (despot-def python-mode-map
    "=i" '(isortify-buffer :wk "(isort) Import Sort buffer")))

;;;; Lua
(use-package lua-mode
  :mode (("\\.lua\\'" . lua-mode))
  :general
  (when gagbo/init-logging (message "Lua/:general"))
  (despot-def lua-mode-map
    "r" '(:ignore t :wk "REPL")
    "rs" '(lua-show-process-buffer :wk "Show")
    "rh" '(lua-hide-process-buffer :wk "Hide")
    "l" '(:ignore t :wk "Load / eval")
    "lb" '(lua-send-buffer :wk "send whole buffer")
    "ll" '(lua-send-current-line :wk "send current line")
    "lf" '(lua-send-defun :wk "send current top-level function")
    "lr" '(lua-send-region :wk "send active region")
    "lF" '(lua-restart-with-whole-file :wk "restart REPL and send whole buffer")
    ))

;;;; Clojure
(use-package clojure-mode
  :mode ("\\.clj\\'" . clojure-mode))
(use-package cider
  :after clojure-mode)

;;;; Markdown
(use-package markdown-mode
  :mode (("README\\.md\\'" . gfm-mode)
	 ("\\.md\\'" . markdown-mode)
	 ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;;;; Latex (Auctex)
;; Auctex is the go-to package to write latex documents in emacs. I have not
;; tested it yet as I'm pretty happy with my vimtex setup (thanks @lervag),
;; but editing/previewing file is more in emacs' philosophy than vim, so
;; maybe it's worth trying.

;; Another point of contention is how well Auctex handle subfiles when using
;; a https://en.wikibooks.org/wiki/LaTeX/Modular_Documents#Subfiles
;; organisation. I need to find a way to emulate the easy
;; =:VimtexToggleMain= switch.

;; Reftex helps with navigation within a .tex file.

;; This code has been taken as a snippet from use-package github page, loading
;; auctex correctly is a lot harder than I think it would be.
(use-package tex
  :ensure auctex
  :custom
  (TeX-auto-save t)
  (TeX-byte-compile t)
  (TeX-clean-confirm nil)
  (TeX-complete-expert-commands t)
  (TeX-debug-bad-boxes t)
  (TeX-debug-warnings t)
  (TeX-electric-escape t)
  (TeX-electric-math t)
  (TeX-electric-sub-and-superscript t)
  (TeX-master 'dwim)
  (TeX-parse-self t)
  (TeX-view-program-selection '((output-pdf "zathura")
				(output-html "xdg-open")))
  (TeX-Omega-command "aleph")
  (TeX-PDF-mode t)
  (TeX-source-correlate-mode t)
  (TeX-source-correlate-start-server t))
(use-package latex
  :ensure auctex
  :mode (("\\.tex\\'" . TeX-latex-mode))
  :config
  (when gagbo/init-logging (message "LaTeX/:config"))
  (require 'font-latex)
  (set-fill-column 80)
  :custom
  (LaTeX-default-style "scrartcl")
  (LaTeX-default-options '("version=last" "paper=A4" "parskip=half"))
  (LaTeX-electric-left-right-brace t)
  :hook ((LaTeX-mode . LaTeX-math-mode)
	 (LaTeX-mode . flyspell-mode)
	 (LaTeX-mode . reftex-mode)
	 (LaTeX-mode . auto-fill-mode)
	 (LaTeX-mode . visual-line-mode)))

;;;; Nix
(use-package nix-mode
  :mode ("\\.nix\\'" . nix-mode))

;;;; Haskell
(use-package haskell-mode
  :mode ("\\.hs\\'" . haskell-mode))
;;;; Emacs Lisp
;; We add a keybinding to start tests using ert, and also to load the current
;; buffer for rapid reloading.
(despot-def emacs-lisp-mode-map
  "t" '(ert :wk "Start tests")
  "l" '(:ignore t :wk "Load / Eval")
  "lf" '(load-file :wk "Load file")
  "lb" '(eval-buffer :wk "Eval buffer")
  "lc" '(eval-last-sexp :wk "Eval last sexp"))
;;;; MTC scripts
;; I built a custom major mode for the markup language used to communicate
;; with the simulation software in the lab.

;; I started making a ftplugin in vim, but creating a proper compiler plugin
;; in vimscript seems a lot harder than in elisp. And on top of this, Emacs
;; seems created to build easy REPLs anyway (and Evil is working)

;; The following code checks for the existence of the =mtc-mode.el= script,
;; and sets things up accordingly if found
(cond ((file-readable-p (emacs-path "etc/mtc-mode.el"))
       (progn
	 (load-file (emacs-path "etc/mtc-mode.el"))
	 (add-to-list 'auto-mode-alist '("\\.mtc\\'" . mtc-mode))))
      (t ()))
;;;; YAML files
(use-package yaml-mode
  :mode (("\\.yml\\'" . yaml-mode)
	 ("\\.yaml\\'" . yaml-mode))
  )
;;;; Shell scripts
;;;;; ShellCheck
(use-package flymake-shellcheck
  :commands flymake-shellcheck-load
  :hook (sh-mode . flymake-shellcheck-load))
;;;;; Fish Mode
(use-package fish-mode
  :mode ("\\.fish\\'" . fish-mode))

;;; languages.el ends here.
