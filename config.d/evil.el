;;; -*- lexical-binding: t -*-
;;; evil.el -- Evil package and helper packages loading
;;; Commentary:
;; This file loads Evil and a few packages like evil-collection
;; And other tpope-y packages

;;; Code:
;;;; Main package installation
(use-package evil
  :init
  (when gagbo/init-logging (message "Evil/:init"))
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  :config
  (when gagbo/init-logging (message "Evil/:config"))

  (evil-mode 1)

  (setq evil-search-module 'evil-search)

  (evil-set-initial-state 'dired-mode 'emacs)
  (evil-set-initial-state 'calendar-mode 'emacs)
  (evil-set-initial-state 'custom-mode 'emacs)
  (evil-set-initial-state 'info-mode 'emacs)

  (evil-define-key 'normal 'global "gc" 'comment-or-uncomment-region)
  (evil-define-key 'normal 'global "gt" 'xref-find-definitions)
  (evil-define-key 'normal 'global "gT" 'xref-find-references)
  (evil-define-key '(normal visual) 'global [escape] 'keyboard-quit)
  (evil-ex-define-cmd "te[rm]" 'ansi-term)
  (define-key evil-ex-search-keymap "\C-r" 'evil-paste-from-register)
  )

;;;; Smaller packages
(use-package evil-org
  :after (evil org)
  :hook ((org-mode . evil-org-mode))
  :config
  (when gagbo/init-logging (message "Evil-org/:config"))
  (add-hook 'evil-org-mode-hook
	    (lambda ()
	      (evil-org-set-key-theme)))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys)
  )

(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode))

(use-package evil-indent-textobject
  :after evil)

(use-package evil-magit
  :after evil)

(use-package evil-matchit
  :after evil)

(use-package evil-nerd-commenter
  :after evil)

(use-package evil-numbers
  :after evil)

(use-package evil-lion
  :after evil)

(use-package evil-tutor
  :after evil)

(use-package evil-visual-mark-mode
  :after evil)

(use-package evil-collection
  :after evil
  :custom
  (evil-collection-setup-minibuffer t)
  :config
  (setq evil-collection-mode-list (delete 'dired evil-collection-mode-list))
  (evil-collection-init))

;;;; C-i
;; Entirely taken from wasamasa config :

;; =C-i= is used in Vim as counterpart to =C-o= for going back and
;; forth in the jump list. It also happens to be interpreted as
;; =TAB=, simply because terminals are a nightmare. Fortunately GUI
;; Emacs can be told to not resolve =C-i= to indentation by defining
;; a function in ~key-translation-map~ that returns the desired key.
;; That way I'm sending a custom =<C-i>= when Evil is active, in
;; normal state and =C-i= (as opposed to the =TAB= key) has been
;; pressed, otherwise =TAB= is passed through.

(defun my-translate-C-i (_prompt)
  (if (and (= (length (this-single-command-raw-keys)) 1)
	   (eql (aref (this-single-command-raw-keys) 0) ?\C-i)
	   (bound-and-true-p evil-mode)
	   (eq evil-state 'normal))
      (kbd "<C-i>")
    (kbd "TAB")))

(define-key key-translation-map (kbd "TAB") 'my-translate-C-i)

(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "<C-i>") 'evil-jump-forward))

;;;; Other bindings
(with-eval-after-load 'evil-maps
  (define-key evil-normal-state-map (kbd "-") 'evil-numbers/dec-at-pt)
  (define-key evil-normal-state-map (kbd "+") 'evil-numbers/inc-at-pt))

(eval-and-compile
  (defun gagbo/evil-unimpaired-insert-newline-above (count)
    "Insert an empty line below point"
    (interactive "p")
    (save-excursion
      (dotimes (i count)
	(evil-insert-newline-above))))

  (defun gagbo/evil-unimpaired-insert-newline-below (count)
    "Insert an empty line below point"
    (interactive "p")
    (save-excursion
      (dotimes (i count)
	(evil-insert-newline-below)))))

(with-eval-after-load 'evil-maps
  (define-key evil-normal-state-map (kbd "[ SPC")
  'gagbo/evil-unimpaired-insert-newline-above)
  (define-key evil-normal-state-map (kbd "] SPC")
  'gagbo/evil-unimpaired-insert-newline-below))

;;; evil.el ends here.
