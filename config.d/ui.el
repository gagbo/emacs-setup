;;; -*- lexical-binding: t -*-
;;; Code:
;;;; Display line number
(global-display-line-numbers-mode t)
;;;; CursorLine
(global-hl-line-mode t)
(advice-add 'load-theme :after (lambda (&rest r) (set-face-underline hl-line-face nil)))
;;;; Outshine
(use-package outshine
  :hook (emacs-lisp-mode . outshine-mode)
  :general
  (when gagbo/init-logging (message "Outshine/:general"))
  (despot-def outline-minor-mode-map
    "o" '(:ignore :wk "Outshine commands")
    "oc" '(outshine-cycle :wk "Cycle")
    "om" '(:ignore :wk "Move/Manipulate subtrees")
    "omj" '(outline-move-subtree-down :wk "Subtree DOWN")
    "omk" '(outline-move-subtree-up :wk "Subtree UP")
    "omh" '(outline-promote :wk "Subtree PROMOTE")
    "oml" '(outline-demote :wk "Subtree DEMOTE")
    "on" '(outshine-narrow-to-subtree :wk "Narrow view to subtree")
    "ow" '(widen :wk "Widen view")
    )
  :config
  (when gagbo/init-logging (message "Outshine/:config"))
  (advice-add 'outshine-narrow-to-subtree :before
	      (lambda (&rest args)
		(unless (outline-on-heading-p t)
		  (outline-previous-visible-heading 1))))
  (define-key outline-minor-mode-map (kbd "M-RET") 'outshine-insert-heading)
  (define-key outline-minor-mode-map (kbd "<backtab>") 'outshine-cycle-buffer)
  (with-eval-after-load 'evil-maps
    (evil-define-key '(normal visual motion) outline-minor-mode-map
      "gh" 'outline-up-heading
      "gj" 'outline-forward-same-level
      "gk" 'outline-backward-same-level
      "gl" 'outline-next-visible-heading
      "gu" 'outline-previous-visible-heading)))

(when (file-exists-p (emacs-path "lisp/outline-ivy/outline-ivy.el"))
  (load (emacs-path "lisp/outline-ivy/outline-ivy.el"))
  (despot-def outline-minor-mode-map
    "oj" '(oi-jump :wk "Jump to outline")))

;;;; Modeline
(use-package doom-modeline
  :after evil
  :init
  (when gagbo/init-logging (message "Doom-ML/:init"))
  (setq doom-modeline-buffer-file-name-style 'truncate-upto-project)
  (setq doom-modeline-python-executable "python")
  :config
  (when gagbo/init-logging (message "Doom-ML/:config"))
  (doom-modeline-mode 1))

;;;; Theme
;; You can use =C-u C-x == to find out which faces are
;; applied to the text at point (under the cursor).
(setq custom-theme-directory "~/.emacs.d/resources/themes")
(setq custom-safe-themes t)
(use-package sourcerer-theme)
(use-package srcery-theme)
(use-package underwater-theme)
(use-package acario-themes
  :load-path "lisp/acario-theme"
  :commands (acario-themes-load-style)
  :general
  (tyrant-def
    "Ts" '(acario-themes-switch-style :wk "Dark/Light switch")))

(acario-themes-load-style 'dark)

;;;; Font
(cond ((eq system-type 'windows-nt)
       (set-face-attribute 'default nil :family "Fira Code" :height 120))
      ((eq system-type 'darwin)
       (set-face-attribute 'default nil :family "Source Code Pro" :height 140))
      (t (set-face-attribute 'default nil :family "Source Code Pro" :height 120)))
(global-prettify-symbols-mode 1)
(use-package all-the-icons)
(use-package all-the-icons-dired
  :after all-the-icons
  :hook (dired-mode . all-the-icons-dired-mode))

;;;; Ruler
(use-package fill-column-indicator
  :init
  (when gagbo/init-logging (message "FCI/:init"))
  (setq fci-rule-width 1)
  (setq fci-rule-column 80)
  (setq fci-rule-color "darkblue")
  :config
  (when gagbo/init-logging (message "FCI/:config"))
  (add-hook 'c-mode-common-hook 'fci-mode))

;;;; Highlight Indent Guide
(use-package highlight-indent-guides
  :init
  (when gagbo/init-logging (message "HIG/:init"))
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-character ?\|)
  :hook
  (prog-mode . highlight-indent-guides-mode)
  )

;;;; Colorize HTML codes

(defvar hexcolour-keywords
  '(("#[abcdef[:digit:]]\\{6\\}"
     (0 (put-text-property (match-beginning 0)
			   (match-end 0)
			   'face (list :background
				       (match-string-no-properties 0)))))))
(defun hexcolour-add-to-font-lock ()
  (interactive)
  (font-lock-add-keywords nil hexcolour-keywords))

;;; ui.el ends here.
