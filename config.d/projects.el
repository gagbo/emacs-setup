;;; -*- lexical-binding: t -*-
(use-package projectile
  :hook (after-init . projectile-mode)
  :general
  (tyrant-def
    "p$t"  'projectile-multi-term-in-root
    "ps"   '(projectile-switch-project :wk "Switch to project")
    "pf"   '(projectile-find-file :wk "Find project file")
    "pk"   '(projectile-kill-buffers :wk "Kill project buffers"))
  )
(use-package editorconfig
  :hook (prog-mode . editorconfig-mode))

;; Global is used to generate tags and call hierarchy in projects. It won't
;; work yet for mtc files because I only created a rule for ctags, but it
;; looks promising at enabling tags navigation almost everywhere
(use-package ggtags
  :hook ((c-mode . ggtags-mode)
	 (c++-mode . ggtags-mode)
	 (java-mode . ggtags-mode)
	 (asm-mode . ggtags-mode))
  :general
  (despot-def ggtags-mode-map
    "g"   '(:ignore t :wk "GGtags")
    "gc"  'ggtags-create-tags
    "gf"  'ggtags-find-file
    "gh"  'ggtags-view-tag-history
    "gp"  'pop-tag-mark
    "gr"  'ggtags-find-reference
    "gs"  'ggtags-find-other-symbol
    "gu"  'ggtags-update-tags)
  )
(add-hook 'after-save-hook #'gagbo/gtags-update-hook)

;;; projects.el ends here.
