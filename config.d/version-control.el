;;; -*- lexical-binding: t -*-
(use-package magit
  :after (general evil)
  :custom
  (magit-blame-echo-style 'margin)
  :general
  (when gagbo/init-logging (message "Magit/:general"))
  (tyrant-def
    "g"   '(:ignore t :which-key "Git")
    "gs"  'magit-status))
(use-package evil-magit
  :after (evil magit))

(use-package diff-hl
  :hook ((dired-mode . diff-hl-dired-mode)
	 (magit-post-refresh . diff-hl-magit-post-refresh))
  :custom (diff-hl-flydiff-delay 0.5)
  :config
  (global-diff-hl-mode 1))

(use-package gitignore-mode
  :mode ("\\.gitignore\\'" . gitignore-mode))

;;; version-control.el ends here.
