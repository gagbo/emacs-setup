;;; -*- lexical-binding: t -*-
;;; Commentary:
;; Various tools in the unix philosophy
;; Debugging, terminals, documention on point
;;; Code:

;;;; Terminal stuff
(use-package shell-pop
  :init
  (when gagbo/init-logging (message "Shell-pop/:init"))
  :custom
  (shell-pop-window-position 'bottom)
  (shell-pop-window-size     35)
  (shell-pop-term-shell      "/bin/bash")
  (shell-pop-shell-type      '("ansi-term"
			       "*ansi-term*"
			       (lambda () (ansi-term shell-pop-term-shell))))
  (shell-pop-full-span       t)
  :general
  (tyrant-def
    "'"   '(shell-pop :which-key "Pop Shell")))

(use-package exec-path-from-shell
  :commands
  (exec-path-from-shell-copy-env exec-path-from-shell-initialize)
  :config
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize))
  :custom
  (exec-path-from-shell-variables '("PYTHONPATH" "PATH" "MANPATH")))

;;;;; Eshell config
(require 'f)

(setq eshell-visual-commands
      '("less" "tmux" "htop" "top" "bash" "zsh" "fish"))

(setq eshell-visual-subcommands
      '(("git" "log" "l" "diff" "show")))

;; Prompt with a bit of help from http://www.emacswiki.org/emacs/EshellPrompt
(defmacro with-face (str &rest properties)
  `(propertize ,str 'face (list ,@properties)))

(eval-and-compile
  (defun gagbo/eshell-abbr-pwd ()
    (let ((home (getenv "HOME"))
	  (path (eshell/pwd)))
      (cond
       ((string-equal home path) "~")
       ((f-ancestor-of? home path) (concat "~/" (f-relative path home)))
       (path))))

  (defun gagbo/eshell-prompt ()
    (let ((header-bg "#161616"))
      (concat
       (with-face (gagbo/eshell-abbr-pwd) :foreground "#008700")
       (if (= (user-uid) 0)
	   (with-face "#" :foreground "red")
	 (with-face "$" :foreground "#2345ba"))
       " "))))

(setq eshell-prompt-function 'gagbo/eshell-prompt)
(setq eshell-highlight-prompt nil)
(setq eshell-prompt-regexp "^[^#$\n]+[#$] ")

(setq eshell-cmpl-cycle-completions nil)

;;;; Debuggers
(use-package realgud
  :after (evil general)
  :custom
  (realgud:pdb-command-name "python -m pdb")
  (realgud:trepan-command-name "python -m trepan"))

(use-package dash-at-point
  :if (eq system-type 'darwin)
  :commands (dash-at-point dash-at-point-with-docset)
  :general
  (tyrant-def
    "d" '(:ignore t :wk "Documentation")
    "dd" 'dash-at-point-with-docset
    "dp" 'dash-at-point))

;;;; Docker
(use-package dockerfile-mode
  :mode "Dockerfile\\'")

(use-package docker
  :commands (docker)
  :general
  (tyrant-def
    "aD" '(docker :which-key "Docker management")))

;;;; Compilation
(use-package meson-mode
  :init
  (when gagbo/init-logging (message "Meson-mode/:init"))
  (add-hook 'meson-mode-hook 'company-mode))

;;;; Home page

;; Using Dashboard package for the startup screen

;; TODO Faces
;; dashboard-banner-logo-title
;;     Highlights the banner title.
;; dashboard-text-banner
;;     Highlights text banners.
;; dashboard-heading
;;     Highlights widget headings.

;; page-break-lines is a dependency of Dashboard
(use-package page-break-lines)
(use-package dashboard
  :init
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
  (setq dashboard-banner-logo-title "This Emacs is Evil"
        dashboard-startup-banner 'logo
	dashboard-items '((recents  . 5)
			  (bookmarks . 5)
			  (projects . 5)
			  (agenda . 5)
			  (registers . 5))
	dashboard-set-heading-icons t
	dashboard-set-file-icons t
	dashboard-set-navigator t
	dashboard-set-init-info t)

  :config
  (dashboard-setup-startup-hook))

;;;; Misc
(use-package key-quiz
  :load-path "lisp/key-quiz"
  :commands (key-quiz)
  :custom
  (key-quiz-matching-regexp "^<?[MCSgz]"))

;;; tools.el ends here.
