;;; -*- lexical-binding: t -*-
;;; Code :
;;;; Todo settings
;; Remainder : the functions I want to use are =(org-toggle-checkbox)= and
;; =(org-todo)=. In order to get the nice progress in the title of the items,
;; I should add =[/]= or =[%]= as cookies in the title of the main item

;; These settings prevents turning a TODO to DONE while there are still
;; tasks to do in the children of the tree
(setq org-enforce-todo-dependencies t
      org-enforce-todo-checkbox-dependencies t)

;; This setting logs the time at which an item was marked as done, and also
;; prompts for a note  that will be added as a closing note. Another option is
;; to use ='time= to get only the timestamp.

;; I use time to begin with because I will probably cycle a lot between states
;; until I am at ease with the different workflow states

;; (setq org-log-done 'note)
(setq org-log-done        'time
      org-log-into-drawer t)

;; This setting sets the different workflow states. For the bug
;; reports, each project should have its own #+SEQ_TODO property
(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)" "DEFERRED(f)" "PHONE" "MEETING")))
(setq org-todo-keyword-faces
      '(("TODO" :foreground "red" :weight bold)
	("NEXT" :foreground "blue" :weight bold)
	("DONE" :foreground "forest green" :weight bold)
	("WAITING" :foreground "orange" :weight bold)
	("HOLD" :foreground "magenta" :weight bold)
	("CANCELLED" :foreground "forest green" :weight bold)
	("DEFERRED" :foreground "forest green" :weight bold)
	("MEETING" :foreground "forest green" :weight bold)
	("PHONE" :foreground "forest green" :weight bold)))

;; This settings allows to fixup the state of a todo item without
;; triggering notes or log.
(setq org-treat-S-cursor-todo-selection-as-state-change nil)

(use-package org-bullets
  :hook (org-mode . org-bullets-mode))

;;;; Capture templates

;; This also defines the org-directory and the different default
;; files, since this is where they're used the most
(setq org-directory "~/org"
      org-agenda-files `(,org-directory
			 ,(concat org-directory "/inbox.org")
			 ,(concat org-directory "/gtd.org")
			 ,(concat org-directory "/rappels.org")
			 ,(concat org-directory "/journal.org"))
      org-default-notes-file (concat org-directory "/inbox.org")
      org-archive-location (concat org-directory "/archive.org::* Depuis %s")
      org-capture-templates
      `(("t" "Tâche [inbox]" entry
	 (file+headline org-default-notes-file "Tâches")
	 "* TODO %i%?\n%U\n%a\n" :clock-in t :clock-resume t)
	("r" "Réponse à envoyer" entry
	 (file ,(concat org-directory "/inbox.org"))
	 "* NEXT Répondre à %:from sur %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
	("n" "note" entry
	 (file ,(concat org-directory "/inbox.org"))
	 "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
	("d" "Rappels" entry
	 (file+headline ,(concat org-directory "/rappels.org") "Rappels")
	 "* %i %^t%? \n Ajouté le %U")
	("m" "Meeting" entry
	 (file ,(concat org-directory "/inbox.org"))
	 "* MEETING avec %? :MEETING:\n%U" :clock-in t :clock-resume t)
	("p" "Phone call" entry
	 (file ,(concat org-directory "/inbox.org"))
	 "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
	("h" "Habitude" entry
	 (file ,(concat org-directory "/inbox.org"))
	 "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n")
	("j" "Journal" entry
	 (file+olp+datetree ,(concat org-directory "/journal.org"))
	 "* %<%H:%M> %^{Title}\n\n%?" :empty-lines 1 :clock-in t :clock-resume t)))

;;;; Refile targets
(setq org-refile-targets `((,(concat org-directory "/gtd.org") :maxlevel . 3)
			   (,(concat org-directory "/un_jour.org") :level . 2)
			   (,(concat org-directory "/rappels.org") :maxlevel . 2)))
(setq org-refile-use-outline-path nil)

;;;; Default tags

(setq org-tag-alist '(
		      (:startgroup . nil)
		      ("BUREAU" . ?b) ("MAISON" . ?h)
		      (:endgroup . nil)
		      ("MEDIA" . ?m)
		      ("KEYBOARDS" . ?k)))
;;;; Agenda commands
(setq org-agenda-custom-commands
      '(("b" "Au Bureau" tags-todo "BUREAU"
	 ((org-agenda-overriding-header "Bureau")))
	("h" "A la maison (home)" tags-todo "MAISON"
	 ((org-agenda-overriding-header "Maison")))
	("w" "Media à consommer (watch)" tags-todo "MEDIA"
	 ((org-agenda-overriding-header "Media")))
	("D" "Revue quotidienne"
	 (
	  (agenda "" ((org-agenda-ndays-to-span 1)
		      (org-agenda-sorting-strategy
		       '((agenda time-up priority-down tag-up)))
		      (org-deadline-warning-days 0)))))))

;;;; Clocking
;; Separate drawers for clocking and logs
(setq org-drawers (quote ("PROPERTIES" "LOGBOOK")))
;; Save clock data and state changes and notes in the LOGBOOK drawer
(setq org-clock-into-drawer t)
;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
(setq org-clock-out-remove-zero-time-clocks t)
;; Clock out when moving task to a done state
(setq org-clock-out-when-done t)
;; Save the running clock and all clock history when exiting Emacs, load it on startup
(setq org-clock-persist t)

;;;; Export backends
(use-package htmlize
  :after org)
(use-package ox-html5slide
  :after org)
(use-package ox-rst
  :after org)
(use-package ox-slack
  :after org)

;;;; Use-package call
(use-package org
  :init
  (when gagbo/init-logging (message "Org/:init"))
  (setq org-src-fontify-natively t)
  (setq org-export-coding-system 'utf-8)
  (setq org-return-follows-link t)
  (setq org-image-actual-width nil)
  (setq org-export-backends '(ascii beamer html latex md))
  :config
  (when gagbo/init-logging (message "Org/:config"))
  (require 'ox-html)
  (require 'ox-md)
  (require 'ox-latex)
  (require 'ox-html5slide)
  (require 'ox-rst)
  (require 'ox-beamer)
  :general
  (when gagbo/init-logging (message "Org/:general"))
  (tyrant-def
    "o"  '(:ignore t :which-key "Org")
    "oa" 'org-agenda
    "ob" 'org-switchb
    "oc" 'org-capture
    "ol" 'org-store-link)
  (despot-def org-mode-map
    "TAB" 'org-cycle
    "A"   'org-archive-subtree-default
    "C"   'org-columns
    "c"   '(:ignore t :wk "Clock")
    "ci"  '(org-clock-in :wk "Clock in")
    "cl"  '(org-clock-in-last :wk "Clock in last")
    "co"  '(org-clock-out :wk "Clock out")
    "d"   'org-deadline
    "e"   'org-export-dispatch
    "i"   'org-toggle-inline-images
    "l"   '(org-babel-load-file :wk "Load file using Babel")
    "r"   'org-refile
    "s"   'org-schedule
    "t"   'org-set-tags-command)
  )

;;;; Default columns
(setq org-columns-default-format "%38ITEM(Details) %TAGS(Context) %7TODO(To Do) %5Effort(Time){:} %6CLOCKSUM{Total}"
      org-global-properties '(("Effort_ALL" . "0 0:10 0:20 0:30 1:00 2:00 3:00 4:00 8:00")))

(setq org-caldav-files org-agenda-files
      org-icalendar-timezone "Europe/Paris")

;;; org.el ends here.
