;;; -*- lexical-binding: t -*-
;;; local_before_init.el -- Machine specific before init file
;;; Commentary:
;; This file is used to set up machine specific settings which
;; should be loaded before initialisation.

;; Do _not_ store secrets here : use .private.el for this
;; Do _not_ set custom variables here : the custom file is set later
;;; Code:

;; Example : set user-mail-address depending on job/mission.
(setq user-mail-address "gagbobada+git@gmail.com")

;; Set url proxies if wifi is specific...

;;; local_before_init.el ends here.
