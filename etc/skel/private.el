;;; -*- lexical-binding: t -*-
;;; .private.el -- Machine specific secrets file
;;; Commentary:
;; This file is used to set up machine specific secrets that
;; should be loaded before initialisation.

;; Do _not_ set custom variables here : the custom file is set later
;;; Code:

;; Example : set freenode-pass for IRC
(setq freenode-pass "")

;;; .private.el ends here.
